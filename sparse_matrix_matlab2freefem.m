function sparse_matrix_matlab2freefem(A)
[row,col,v] = find(A);
[row_s,idx]=sort(row);
col_s=col(idx);
v_s=v(idx);
[n,m]=size(A);
n_val=length(v);
fid = fopen('freefem_matrix.txt','wt');
fprintf( fid,'# Sparse Matrix (Morse) \n# first line: n m (is symmetic) nbcoef \n# after for each nonzero coefficient:   i j a_ij where (i,j) %s  {1,...,n}x{1,...,m} \n','\in');
fprintf( fid,'%d %d %d  %d\n',[n,m,0,n_val]);
for i=1:length(v)
 fprintf(fid,'%9d%9d %d\n',[row_s(i),col_s(i),v_s(i)]);
end
 fprintf(fid,'\n');
fclose(fid);
end