# README #

This function permits to write a sparse Matlab matrix in a .txt file without making it full.
The .txt file is ready to be loaded in Freefem++ with just one command.
```ifstream file("freefem_matrix.txt");```
```matrix A;```
```file>>A;```